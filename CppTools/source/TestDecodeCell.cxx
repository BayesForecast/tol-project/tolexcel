#include <iostream>

size_t decodeColumn(const char * cellName, size_t length )
{
  if ( !length ) return 0;
  char c = cellName[ --length ];
  if ( c < 'A' || c > 'Z')
    return 0;
  return (c - 'A' + 1) + decodeColumn( cellName, length ) * 26;
}

bool decodeCoordinates(const char * cellName, size_t &row, size_t &column )
{
  size_t i = 0;

  // decode column A-based
  char ch = cellName[ i ];
  while ( ch >= 'A' && ch <= 'Z' ) 
    { 
    ch = cellName[ ++i ];
    }
  column = decodeColumn( cellName, i );
  if (!ch || !column) 
    {
    return false;
    }
  // decode row 1-based
  row = 0;
  //size_t i0 = i;
  //while ((i == i0 && ch >= '1' && ch <= '9') || (ch >= '0' && ch <= '9'))
  while (ch >= '0' && ch <= '9')
    {
    row = row * 10 + ch - '0';
    ch = cellName[++i];
    }
  if (ch) 
    {
    return false;
    }
  // cell coordinates are 0-based internally
  --row;
  --column;
  return true;
}

int TestCellName(const char * cellName)
{
  size_t r = 0, c = 0;
  std::cout << "Testing: " << cellName << " --> ";
  bool status = decodeCoordinates(cellName, r, c);
  if (status)
    {
    std::cout << "Decoded to: (" << r << ", " << c << ")\n"; 
    }
  else
    {
    std::cout << "Decoding failed\n";
    }
  return 0;
}

int main(int argc, char * argv[])
{

  TestCellName("A1xx");
  TestCellName("A1");
  TestCellName("A10");
  TestCellName("A11");
  TestCellName("X1010");
  TestCellName("XX1010");
  TestCellName("A");
  TestCellName("A01");
  TestCellName("A0101");
  TestCellName("A000101");
  
  return 0;
}

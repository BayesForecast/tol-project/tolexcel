//////////////////////////////////////////////////////////////////////////////
Class @EntityFrame
//////////////////////////////////////////////////////////////////////////////
{
  Set fields = Copy(Empty);
  Set entities = Copy(Empty);
  Set joins = Copy(Empty);

  Real buildFromLines(Set lines)
  {
    Set fields := @Field::CreateFieldsFromLine(lines);
    Set aux1 = Classify(fields,Real(@Field a, @Field b)
    {
      Compare(a::entity,b::entity)
    });
    Set entities := EvalSet(aux1,@Entity(Set cls)
    {
      @Entity::CreateFromFields(cls)
    });
    Real SetIndexByName(entities);
    True
  };

  Static @EntityFrame CreateFromLines(Set lines)
  {
    @EntityFrame entityFrame = [[Set fields = Copy(Empty)]];
    Real entityFrame::buildFromLines(lines);
    entityFrame
  };

  Set getAllCases(Real void)
  {
    EvalSet(entities,Set(@Entity ent)
    {
      Eval(ent::identify+"=ent::getCases(?)")
    })
  };

  @Entity crossJoin(@Entity A, @Entity B)
  {
    Set cA = A::getCases(?);
    Set cB = B::getCases(?);
    Set nAB = A::getColNames(?) << B::getColNames(?);
    Text idAB = If(A::identify==B::identify,A::identify,
      A::identify+"_x_"+B::identify);
    Set idfAB = EvalSet(nAB,Text(Text name)
    {
      idAB+"."+name
    }); 
    Set cAB = 
    [[ idfAB ]]<<
    EvalSet(CartProd(cA,cB),Set(Set pair)
    {
      pair[1]<<pair[2]
    });
    @EntityFrame ABframe = @EntityFrame::CreateFromLines(Traspose(cAB));
    ABframe::entities[1]
  };

  @Entity buildIndexedJoin(@Entity A, @Entity B, Set commonFields)
  {
    Set crossFields = CartProd(A::fields,B::fields);
    Set commonFields = Select(crossFields,Real(Set pair)
    {
      (pair[1])::name == (pair[2])::name
    });
    Set commonColNames = EvalSet(commonFields,Text(Set pair)
    {
      (pair[1])::name
    });
    Set nA = A::getColNames(?);
    Set nBidx = Select(Range(1,B::cols,1),Real(Real j)
    {
      Not((B::fields[j])::name <: commonColNames)
    });
    Set nB = ExtractByIndex(B::getColNames(?),nBidx);
    Set nAB = nA << nB;
    Text idAB = If(A::identify==B::identify,A::identify,
      A::identify+"_x_"+B::identify);
    Set idfAB = EvalSet(nAB,Text(Text name)
    {
      idAB+"."+name
    }); 
    Set cf = commonFields[1];
    Set cIdx = SetConcat(For(1,Card((cf[1])::key),Set(Real ka)
    {
      Text kw = (cf[1])::key[ka];
      Set a = (cf[1])::searchValue(kw);
      Set b = (cf[2])::searchValue(kw);
      Set c = CartProd(a,b)
    }));
    Set cA = A::getCases(?);
    Set cB = B::getCases(?);
    Set cAB = 
    [[ idfAB ]]<<
    EvalSet(cIdx,Set(Set pair)
    {
      Set a = cA[ pair[1] ];
      Set b = cB[ pair[2] ];
      a<<ExtractByIndex(b,nBidx)
    });
    @EntityFrame ABframe = @EntityFrame::CreateFromLines(Traspose(cAB));
    ABframe::entities[1]
  };

  @Entity buildJoin(@Entity A, @Entity B)
  {
  //WriteLn("TRACE [buildJoin] "<<A::identify+" x "+B::identify);
    If(A::identify==B::identify,
    {
    //WriteLn("TRACE [buildJoin] Found "<<A::identify);
      entities[A::identify]
    },
    {
      Text idAB = A::identify+"_x_"+B::identify;
      Real posEnt = FindIndexByName(entities,idAB);
    //WriteLn("TRACE [buildJoin] ["<<idAB+"]->"<<posEnt);
      If(posEnt,
      {
      //WriteLn("TRACE [buildJoin] Found "<<(entities[posEnt])::identify);
        entities[posEnt]
      },
      {
      //WriteLn("TRACE [buildJoin] Building "<<idAB+" 1 ");
        @NameBlock ent = [[If(A::rows>B::rows, 
        {
        //WriteLn("TRACE [buildJoin] reverting entities ...");
          buildJoin(B,A)
        },
        {
        //WriteLn("TRACE [buildJoin] Building "<<idAB+" 2 ");
          Set crossFields = CartProd(A::fields,B::fields);
        //WriteLn("TRACE [buildJoin] Building "<<idAB+" 3 ");
          Set commonFields = Select(crossFields,Real(Set pair)
          {
            (pair[1])::name == (pair[2])::name
          });
        //WriteLn("TRACE [buildJoin] Building "<<idAB+" 4 ");
          If(Card(commonFields), 
            buildIndexedJoin(A,B,commonFields),
            crossJoin(A,B))
        })]];
      //WriteLn("TRACE [buildJoin] Built "<<$ent::identify+" 5 ");
        Set If(!FindIndexByName(entities,$ent::identify), 
          Append(entities,ent,True));
        $ent
      })
    })
  };

  @Entity join(Text a, Text b)
  {
    buildJoin(entities[a],entities[b])
  };

  @Entity buildJoinAll(Set entitySet)
  {
    @Entity ent = Case(  
    Card(entitySet)==0, 
    {
      @Entity::CreateEmpty(?)
    },
    Card(entitySet)==1, 
    {
      entitySet[1]
    },
    Card(entitySet)==2, 
    {
      buildJoin(entitySet[1],entitySet[2])
    },
    Card(entitySet)>2, 
    {
      @Entity ent12 = buildJoinAll([[ entitySet[1],entitySet[2] ]]);
      Set rest = ExtractByIndex(entitySet,Range(3,Card(entitySet),1));
      buildJoinAll([[ent12]]<<rest)
    })
  };

  Set expand(Set reg)
  {
    Set regFields = Copy(Empty);
    Set regEntities = Copy(Empty);
    //Anything cell = reg[1];
    Set regInfo = EvalSet(reg,Set(Anything cell)
    {
      Text gr = Grammar(cell);
      Set cellFields = Copy(Empty);
      Real If(gr=="Real",False,
      {
        Real p = -1;
        While(p,
        {
          Real p0 = TextFind(cell,"{",p);
          If(!p0, p:=0,
          {
            Real p := TextFind(cell,"}",p0);
            Text fld = Sub(cell,p0+1,p-1);
            @Field f = fields[fld];
            Set If(!FindIndexByName(cellFields,fld),
              Append(cellFields,[[fields[fld] ]],True));  
            Set If(!FindIndexByName(regFields,fld),
              Append(regFields,[[ fields[fld] ]],True));
            Set If(!FindIndexByName(regEntities,f::entity),
              Append(regEntities,[[ entities[f::entity] ]],True));
            p
          })
        });
        True
      });
      [[cell, gr, cellFields]]
    });
    @Entity ent = buildJoinAll(regEntities);
    Set fieldReplace = SetConcat(EvalSet(regFields,Set(@Field fld)
    {
      @NameBlock fld_ = Select(ent::fields,Real(@Field f)
      {
        f::name == fld::name
      });
      If(fld::identify==$fld_::identify,Empty,
      [[ [[Text "{"+fld::identify+"}", Text "{"+$fld_::identify+"}"]] ]])
    }));
    
    Set reg_ = EvalSet(reg,Anything(Anything cell)
    {
      If(Grammar(cell)!="Text",cell,
      {
        ReplaceTable(cell,fieldReplace)
      }) 
    });
    ent::expand(reg_)
  } 
};

Real _unused_entity_frame = ?;

/* */
